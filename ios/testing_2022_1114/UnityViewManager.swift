//
//  UnityViewManager.swift
//  testing_20221110_rn
//
//  Created by Chris Wong on 10/11/2022.
//

import Foundation
import SwiftUI
@objc(UnityViewManager) class UnityViewManager: RCTViewManager {
  
  
  
  @objc func pressed() {
      var alertView = UIAlertView()
    alertView.addButton(withTitle: "Ok")
      alertView.title = "title"
      alertView.message = "message"
      alertView.show()
  }

  
  override func view() -> UIView! {
    //    let label = UILabel()
    //    label.text = "Swift Counter"
    //    label.textAlignment = .center
    //    return label
    
    let button = UIButton();
    button.setTitle("✸", for: .normal);
    button.setTitleColor(.blue, for: .normal);
    button.frame = CGRect(x: 15, y: -50, width: 300, height: 500);
    button.addTarget(self, action: #selector(Unity.shared.show), for: .touchUpInside)
   

  
    return button;
    //    if #available(iOS 13.0, *) {
    //          let vc = UIHostingController(rootView: ContentView())
    //          return vc.view;
    //        } else {
    //          return nil;
    //          // Fallback on earlier versions
    //        }
    //
    //  }
    
    //  override func view() -> UIView! {
    //
    //    if (@available(iOS 13.0, *)) {
    //           SwiftUIButtonProxy *proxy = [[SwiftUIButtonProxy alloc] init];
    //           return proxy.vc.view;
    //       }
    //       return nil;
    
    //    if #available(iOS 13.0, *) {
    //      let vc = UIHostingController(rootView: ContentView())
    //      return vc.view;
    ////      addChild(vc)
    ////      vc.view.frame = self.view.frame
    ////      view.addSubview(vc.view)
    ////      vc.didMove(toParent: self)
    //    } else {
    //      return nil;
    //      // Fallback on earlier versions
    //    }
    
  }
  //  override func viewDidLoad() {
  //      super.viewDidLoad()
  //
  //      let vc = UIHostingController(rootView: ContentView())
  //      addChild(vc)
  //      vc.view.frame = self.view.frame
  //      view.addSubview(vc.view)
  //      vc.didMove(toParent: self)
  //  }
  
  
  
}
